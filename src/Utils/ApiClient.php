<?php

namespace App\Utils;

use Psr\Log\LoggerInterface;
use GuzzleHttp\Client as Guzzle;
use App\Utils\ApiResources as ApiResources;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Guzzle\Http\Exception\ClientErrorResponseException;

class ApiClient {

  /**
   * Parameters
   * 
   * @var ParameterBagInterface
   */
  private $params;

  /**
   * Logger
   * 
   * @var LoggerInterface
   */
  private $logger;

  /**
   * Guzzle http client
   * 
   * @var Guzzle
   */
  protected $client;

  public function __construct(ParameterBagInterface $params, LoggerInterface $logger) {
    $this->params = $params;
    $this->logger = $logger;    
    $this->client = new Guzzle(['base_uri' => $this->params->get('DESKPRO_API_URL'), 'headers' => $this->getHeaders()]);
  }

  private function getHeaders() {
    return [
      'Authorization' => 'key ' . $this->params->get('DESKPRO_API_KEY')
    ];
  }

  public function getResources(ApiResources $resource) {
    try {
      $response = $this->client->get($resource->getValue());
      return json_decode($response->getBody(), $assoc = true, $depth = 512, JSON_THROW_ON_ERROR);
    } catch (Exception $error) {
      return array([
        "data" => array()
      ]);
    }
  }

  public function getOneResource(ApiResources $resource, int $id) {
    try {
      $response = $this->client->get($resource->getValue().'/'.$id);
      return json_decode($response->getBody(), $assoc = true, $depth = 512, JSON_THROW_ON_ERROR);
    } catch (Exception $error) {
      return array([
        "data" => array()
      ]);
    }
  }

  public function updateOneResource(ApiResources $resource, int $id, array $data) {
    try {
      $this->logger->info("Update", $data);
      $response = $this->client->put($resource->getValue().'/'.$id,[
        "form_params" => $data,
        'debug' => true,
      ]);
      return true;
    } catch (\Exception $error) {
      $this->logger->error("Error", ["error" => $error]);
      return false;
    }
  }
}