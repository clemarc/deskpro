<?php

namespace App\Utils;

use MyCLabs\Enum\Enum;

/**
 * Action enum
 */
class ApiResources extends Enum
{
    private const USERS = 'people';
    private const ORGS = 'organizations';
    private const TICKETS = 'tickets';
}