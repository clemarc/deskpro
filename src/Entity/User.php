<?php
namespace App\Entity;

class User {

  /**
   * id
   * 
   * @var int
   */
  public $id;

  /**
   * First name
   * 
   * @var string
   */
  public $first_name;

  /**
   * First name
   * 
   * @var string
   */
  public $last_name;

  /**
   * Display name
   * 
   * @var string
   */
  public $display_name;

  public function getApiData(): array {
    return [
      "first_name" => $this->first_name,
      "last_name" => $this->last_name,
    ];
  }

  public static function fromApi($userApi): User {
    $user = new User();
    $user->id = $userApi['id'];
    $user->first_name = $userApi['first_name'];
    $user->last_name = $userApi['last_name'];
    $user->display_name = $userApi['display_name'];
    return $user;
  }
  
}