<?php
namespace App\Entity;

class Organization {

  /**
   * id
   * 
   * @var int
   */ 
  public $id;

  /**
   * Name
   * 
   * @var string
   */
  public $name;

  /**
   * Summary
   * 
   * @var string
   */
  public $summary;

  public function getApiData(): array {
    return [
      "name" => $this->name,
      "summary" => $this->summary,
    ];
  }

  public static function fromApi(array $organizationApi): Organization {
    $organization = new Organization();
    $organization->id = $organizationApi['id'];
    $organization->name = $organizationApi['name'];
    $organization->summary = $organizationApi['summary'];
    return $organization;
  }
  
}