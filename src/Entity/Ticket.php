<?php
namespace App\Entity;

class Ticket {

  /**
   * id
   * 
   * @var int
   */ 
  public $id;

  /**
   * First name
   * 
   * @var string
   */
  public $email;

  /**
   * First name
   * 
   * @var string
   */
  public $subject;

  public function getApiData(): array {
    return [
      "subject" => $this->subject,
    ];
  }

  public static function fromApi(array $ticketApi): Ticket {
    $ticket = new Ticket();
    $ticket->id = $ticketApi['id'];
    $ticket->email = $ticketApi['person_email'];
    $ticket->subject = $ticketApi['subject'];
    return $ticket;
  }
  
}