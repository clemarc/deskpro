<?php
namespace App\Services;

use Psr\Log\LoggerInterface;
use App\Utils\ApiClient as ApiClient;
use App\Utils\ApiResources as ApiResources;
use App\Services\AbstractService as AbstractService;

class Organization extends AbstractService {

  public function __construct(ApiClient $apiClient, LoggerInterface $logger) {
    parent::__construct($apiClient, $logger);
    $this->resource = ApiResources::ORGS(); 
  }

}