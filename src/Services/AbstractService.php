<?php

namespace App\Services;
use Psr\Log\LoggerInterface;
use App\Utils\ApiClient as ApiClient;
use App\Utils\ApiResources as ApiResources;

class AbstractService {

  /**
   * Logger
   * 
   * @var LoggerInterface
   */
  private $logger;

  /**
   * The API Client
   * 
   * @var ApiClient
   */
  private $apiClient;

  /**
   * The API Resource
   * 
   * @var ApiResources
   */
  protected $resource;

  public function __construct(ApiClient $apiClient, LoggerInterface $logger) {
    $this->apiClient = $apiClient;
    $this->logger = $logger;
  }

  public function getAll(): array {
    $res = $this->apiClient->getResources($this->resource);
    return $res['data'];
  }

  public function getOne(int $id): array {
    $res = $this->apiClient->getOneResource($this->resource, $id);
    return $res['data'];
  }

  public function updateOne(int $id, array $data): bool {
    $res = $this->apiClient->updateOneResource($this->resource, $id, $data);
    return $res;
  }

}