<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

use App\Services\Organization as OrganizationService;
use App\Entity\Organization;

class OrganizationController extends AbstractController
{

  /**
  * @Route("/orgs", methods={"GET"}))
  */
  public function index(OrganizationService $organizationService)
  {
    $organizations = array_map('App\Entity\Organization::fromApi',$organizationService->getAll());

    return $this->render('orgs.html.twig', ['orgs' => $organizations]);
  }

  /**
  * @Route("/orgs/{id}", name="org_details" ,requirements={"id"="\d+"}, methods={"GET", "POST"})
  */
  public function userDetails(int $id, OrganizationService $organizationService, Request $request)
  {
    $organization = Organization::fromApi($organizationService->getOne($id));

    $organization_form = $this->createFormBuilder($organization)
      ->add('name', TextType::class)
      ->add('summary', TextType::class)
      ->add('save', SubmitType::class, ['label' => 'Save ticket'])
      ->getForm();

    $organization_form->handleRequest($request);

    if ($organization_form->isSubmitted() && $organization_form->isValid()) {
        $organizationService->updateOne($id, $organization->getApiData());
        return $this->redirectToRoute('org_details', ['id' => $id]);
    }

    return $this->render('org_details.html.twig', [
      'org' => $organization,
      'form' => $organization_form->createView()
    ]);
  }

}
