<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

use App\Services\User as UserService;
use App\Entity\User as User;

class UserController extends AbstractController
{

  /**
  * @Route("/users", methods={"GET"}))
  */
  public function index(UserService $userService)
  {
    $users = array_map('App\Entity\User::fromApi', $userService->getAll());
    return $this->render('users.html.twig', ['users' => $users]);
  }

  /**
  * @Route("/users/{id}", name="user_details" ,requirements={"id"="\d+"}, methods={"GET", "POST"})
  */
  public function userDetails(int $id, UserService $userService, Request $request)
  {
    $user = User::fromApi($userService->getOne($id));

    $user_form = $this->createFormBuilder($user)
      ->add('first_name', TextType::class)
      ->add('last_name', TextType::class)
      ->add('save', SubmitType::class, ['label' => 'Save user'])
      ->getForm();

    $user_form->handleRequest($request);

    if ($user_form->isSubmitted() && $user_form->isValid()) {
        $updated_user = $user_form->getData();
        $userService->updateOne($id, $user->getApiData());
        return $this->redirectToRoute('user_details', ['id' => $id]);
    }

    return $this->render('user_details.html.twig', [
      'user' => $user,
      'form' => $user_form->createView()
    ]);
  }

}
