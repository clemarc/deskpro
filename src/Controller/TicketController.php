<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\Ticket as TicketService;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Ticket;

class TicketController extends AbstractController
{

  /**
  * @Route("/tickets", methods={"GET"}))
  */
  public function index(TicketService $ticketService)
  {
    $tickets = array_map('App\Entity\Ticket::fromApi',$ticketService->getAll());

    return $this->render('tickets.html.twig', ['tickets' => $tickets]);
  }

  /**
  * @Route("/tickets/{id}", name="ticket_details" ,requirements={"id"="\d+"}, methods={"GET", "POST"})
  */
  public function userDetails(int $id, TicketService $ticketService, Request $request)
  {
    $ticket = Ticket::fromApi($ticketService->getOne($id));

    $ticket_form = $this->createFormBuilder($ticket)
      ->add('subject', TextType::class)
      ->add('save', SubmitType::class, ['label' => 'Save ticket'])
      ->getForm();

    $ticket_form->handleRequest($request);

    if ($ticket_form->isSubmitted() && $ticket_form->isValid()) {
        $ticketService->updateOne($id, $ticket->getApiData());
        return $this->redirectToRoute('ticket_details', ['id' => $id]);
    }

    return $this->render('ticket_details.html.twig', [
      'ticket' => $ticket,
      'form' => $ticket_form->createView()
    ]);
  }

}
