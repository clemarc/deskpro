# deskpro
Deskpro test

# Requirements
* PHP 7.3 for json thrown on error

# Usage
Run the following commands
~~~~
composer install
bin/console s:r
~~~~
Open the URL displayed in terminal :)

# Comments
* Missing unit tests
* Login is missing
* Controllers could be refactored like Services
* UX wise, missing notifications for success/error on update objects
